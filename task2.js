const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")
const getAcronym = async function() {
  var acronym = randomstring.generate(3).toUpperCase();
  let data = await got(inputURL+acronym);
  output.definitions.push(data.body)
  if (output.definitions.length < 10) {
      console.log("calling looping function again");
      await getAcronym();
    }
  return output;
}
console.log("calling looping function");
getAcronym().then(res => {
  console.log(res);
  jsonfile.writeFile(outputFile, res, {spaces: 2}, function(err) {
    console.log("All done!");
  })
});
console.log("saving output file formatted with 2 space indenting");
