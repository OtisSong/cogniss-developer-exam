const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const outputFile = "output2.json"
const inputFile = "input2.json"

const Task1 = function(str){
    const randomStr =  randomstring.generate(5)
    const result = str.split("").reverse().join("").concat(randomStr,"@gmail.com")
    return result;
}

jsonfile.readFile(inputFile, function(err, body){
    const arr = body.names;
    let result = {};
    result.email = [];
    arr.forEach(element=>result.email.push(Task1(element)))
    jsonfile.writeFile(outputFile, result, {spaces: 2}, function(err) {
        console.log("All done!");
      });
})
